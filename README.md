# Eksempler til leksjon 1

## Forutsetninger
* Linux eller MacOS
* IDE'en [juCi++](https://gitlab.com/cppit/jucipp)

## Last ned og kjør:
```sh
git clone https://gitlab.com/ntnu-iini4003/examples1
juci examples1
```

Deretter, åpne eksempelfilen du vil kjøre, for eksempel eksempel.cpp, og velg *Compile
and Run* i *Project* menyen.

## Forklaring av vesentlig annet innhold i mappen:
* **CMakeLists.txt** - denne filen inneholder prosjektinnstillingene. Her kan du legge
til et eller flere program du vil kompilere, og de cpp-filene som trengs for
å kompilere programmene. Dette blir gjort gjennom *add_executable* funksjonen.
* **.clang-format** - oppsett av stilformatering av kildekoden.
* **build/** - denne mappen inneheld de kjørbare filene etter kompilering.
